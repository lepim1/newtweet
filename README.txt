This is a blog developed by Leonardo Pimentel Ferreira, lepim1@morgan.edu
Version: 1.0

The application is hosted on Heroku. Below are the technologies that 
I have used for both Heroku and for building the website. 
This web system is responsive, so users can access it in small devices.

Clone it: https://lepim1@bitbucket.org/lepim1/newtweet.git

- Heroku
    ClearDB: MySQL client 
    SendGrid 2.0.5: SMTP client

- build the pages:
    PHP  >= 5.3			#Server-side language. (I used MVC and Object-Oriented PHP)
    Bootstrap 3.3.1		#Front-end framework, responsive.
    Jquery 1.11.1 and Ajax	#JavaScript framework
    Holder.js 2.4.1		#JavaScript framework for redering images on the client-side
    Jquery Validate v1.13.1	#Jquery framework for validating forms
    Telize			#for IP Geolocation
    reCAPTCHA 2			#to protect the website from spam

- Others:
    Composer:                   #it handles dependencies that are used by the PHP system    
    BitBucket and Git           #Code hosts with version control.
