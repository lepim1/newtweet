///ENABLES POPOVER AND TOOLTIP
$(function () {
    $('[data-toggle="popover"]').popover()        
    $('[data-toggle="tooltip"]').tooltip()
})

/**
 * IT IS AN AUXILIAR FUNTION TO DELAY AN ACTION
 * @param {type} MILISECONDS
 * @type Function|Function
 */
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

/**
 * SEARCH FOR USERS
 * @param {type} param
 */
$('#user').keyup(function(){
    $('input.popupsearchusers').popover('destroy');
    if($('#user').val().length >= 3){                
        delay(function(){               
            e = $('input.popupsearchusers');                                    
            $.get(e.data('poload'),$("#formsearchuser").serialize(),function(d) {                
                var html = formatSearchResult(d);
                e.popover({content: html, html: true, container: 'body'}).popover('show');            
            }).fail(function(e){                
                //var obj = JSON.parse(e);                
                //console.log(JSON.stringify(obj));
            });
        }, 1000 );
    }               
});

/**
 * FORMAT A LIST OF USERS AS A RESULT OF THE USER SEARCHING
 * @param {type} users
 * @returns {String}
 */
function formatSearchResult(users) {        
    var html = '<div class="list-group">';
    var count = 0;
    for (var user in users) {
        if (users.hasOwnProperty(user)) {                                    
            count += 1;
            var val = users[user];
            html += '<a class="list-group-item" href="mural.php?perfil=' + user+ '">'+
                        '<div class="media" id="'+ user + '">' +                    
                            '<div class="media-left">'+                            
                                '<span class="glyphicon glyphicon-user" aria-hidden="true" style="font-size: 4em;"></span>'+                            
                            '</div>'+
                            '<div class="media-body">'+
                                '<h5>'+val[0]+'</h5>'+
                                '<small>'+val[1]+'</small>'+
                            '</div>'+                                       
                        '</div>'+
                    '</a>';                
        }
    }
    if(count === 0){
        html += 'Nenhum resultado encontrado!';
    }
    html += "</div>";
    return html;
}

/**
 * HIDES A POPOVER WHEN THERE IS A CLICK ON THE BODY PORTION
 * @param {type} param1
 * @param {type} param2
 */
$('html').on('click', function (e) {
    $('.pop').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $('.pop').popover('destroy');
            showingpopover = false;
        }                
    });
});