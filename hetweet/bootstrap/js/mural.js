///ENABLES POPOVER AND TOOLTIP
$(function () {
    $('[data-toggle="popover"]').popover()        
    $('[data-toggle="tooltip"]').tooltip()
})

//VARS THAT CONTROLS THE NOTIFICATIONS AND UPDATES
var datelastupdate = 0;
var showingpopover = false;
var notification_content = "";

/**
 * GETS A LIST OF NOTIFICATIONS AND POPULATES A POPOVER
 * @param {BOOLEAN} show = SHOW OR NOT
 * @returns {undefined}
 */
function getNotifications(show){
    var e=$('a.popupnotifications');   
    var op = getOpNotification();
    $.post(e.data('poload'),{op: op, datelastupdate: datelastupdate},function(d) {
        d = JSON.parse(d);                                   
        notification_content = d.html + notification_content; 
        e.popover({content: '<div class="list-group">'+notification_content+'</div>', html: true, container: 'body'});
        if(show){
            e.popover('show');
            showingpopover = true;
        }        
        if(op == 1 && show == false){
            if(d.count>0){               
                $("#numnotifications").text('+');
            }
        }
        datelastupdate = getTimezoneString();                

    }); 
}

/**
 * UPDATES THE NOTIFICATIONS FOR EACH 10 SECONDS 
 * @returns {undefined}
 */
setInterval(function(){
    if($(".popupnotifications").length){//if the element exists
        if(showingpopover === false){        
            getNotifications(showingpopover);
        }
    }
},10000);

/**
 * Solving problem with black dots surrounding links on the top navbar on firefox 
 */
$('a').focus(function(){
    $(this).blur()
})
/**
 * EVENT THAT IS TRIGGERED WHEN THE NOTIFICATION BUTTON IS CLICKED
 * @param {type} param
 */
$('a.popupnotifications').click(function() {  
    if(showingpopover === true){
        $('a.popupnotifications').popover('destroy');
        showingpopover = false;
        return false;
    }
    $("#numnotifications").text('');
    showpopover = true;
    getNotifications(showpopover);
});

/*
 * SIMPLE FUNCTION THE RETURNS 2 IF IT IS THE FIRST TIME THAT THE NOTIFICATIONS ARE BEING UPDATED OR 1 OTHERWISE
 */
function getOpNotification(){
    var op;
    if(datelastupdate === 0){
        op = 2;
    }else{
        op = 1;
    }
    return op;
}

/**
 * SENDS A MESSAGE AND SHOWS IT ON THE MURAL
 */
$('#btsendmessage').click(function(e){ 
    var message = $("#postmessage");
    if(message.val() === ""){
        return false;
    }
    data = $("#formsendmessage").serialize()+"&op=postnewmessage";            
    var form = $("#formsendmessage");                        
    $.ajax({
        type: "post",
        url: "../controller/ctweet.php",
        data: data,
    }).done(function(result) { 
        var obj = JSON.parse(result);
        if(obj.response === true){            
            obj['name'] = $("#fullname").text();
            obj['comment'] = message.val();
            html = formatTweetHTML(obj);              
            $(html).insertAfter(".page-header");
            $(form).closest('form').find("input[type=text], textarea").val("");            
            return true;
        }else{
            if(obj.response === false){
                alert("Erro");                
            }
        }
        return false;
    }).fail(function(){
        alert("Erro");
    });
});

$('#follow').click(function(){
    var follow = $(this);
    followToggle("follow",follow);     
});

$('#unfollow').click(function(){   
    var unfollow = $(this);
    followToggle("unfollow",unfollow);    
});

/**
 * FOLLOW OR UNFOLLOW A USER
 * @param {STRING} follow or unfollow 
 * @param {DOM ELEMENT} THE BUTTON THAT HAS BEEN CLICKED 
 * @returns {undefined}
 */
function followToggle(op,action){    
    var perfil = getPerfilFromURL();
    if(!isNaN(perfil)){        
        $.ajax({
            type: "get",
            url: "../controller/cfollow.php",
            data: "op="+op+"&perfil="+perfil,
        }).done(function(result) {            
            var obj = JSON.parse(result);
            if(obj.response){                
                $(action).hide();
                if(op == "follow"){
                    $("#unfollow").show();
                }else{
                    if(op == "unfollow"){
                        $("#follow").show();
                    }
                }
            }
            return true;
        }).fail(function(){
            alert("Erro");
        });
    }
}

/**
 * GET THE PERFIL OR MURAL FROM THE URL
 * @param {type} param
 * @returns {unresolved}
 */
function getPerfilFromURL(param)
{
    //var pathname = window.location.pathname; // Returns path only
    var url      = window.location.href;     // Returns full URL            
    //var params = url.split('/');
    //return params[params.length-1].replace("#","");
        
    return url.split('perfil=')[1];
;
}

/**
 * DELEGATES A CLICK EVENT FOR THE LIKE BUTTON. THIS IS BECAUSE THE ELEMENT MAY NOT HAVE BEEN CREATED SO A SIMPLE CLICK EVENT WILL NOT WORK.
 */
$("body").on('click', "a.like",function(){    
    var like = $(this);
    var tweet = parseInt(like.attr('id'));   
    var op = "like";
    likeToggle(op, tweet, like);
});

$("body").on('click', "a.unlike",function(){    
    var like = $(this);
    var tweet = parseInt(like.attr('id'));   
    var op = "unlike";
    likeToggle(op, tweet, like);
});

/**
 * LIKE OR DISLIKE A TWEET
 * @param {STRING} like or unlike
 * @param {INT} tweet 
 * @param {DOM ELEMENT} THE LIKE OR DISLIKE BUTTON
 * @returns {undefined}
 */
function likeToggle(op, tweet, elementlike){
    $.ajax({
        type: "get",
        url: "../controller/clike.php/"+op,
        data: "op="+op+"&tweet="+tweet+"&mural="+$("#perfil").val(),
    }).done(function(result) {              
        var obj = JSON.parse(result);
        if(obj.response === true){
            if(op=="like"){
                elementlike.removeClass("like");   
                elementlike.addClass("unlike");
                elementlike.text("Você gostou disso. (Clique se não gostou)");                    
            }else{
                elementlike.addClass("like");
                elementlike.removeClass("unlike");                     
                elementlike.html('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Clique se gostou'); 
            }
        }else{
            if(obj.response === false){
                alert("Você precisa acessar a sua conta para realizar a operacão");
            }
        }
    }).fail(function(){
        alert("Erro");
    });
}

/**
 * COMMENTS A TWEET
 */
$("body").on('click', '.btcomment', function() {    
    var btcomment = $(this);    
    var comment = btcomment.siblings(".txtcomment").val();         
    if(comment === ""){
        return false;
    }
    var isfirstcomment = btcomment.parents(".media").length === 1;    
    if(isfirstcomment){
        var tweet = btcomment.siblings(".txtcomment").attr("name");
        tweet = tweet.substring(6,tweet.length);
    }else{                
        var tweet = $(this).parents('.media-body').last().children(".likebox").children("a").attr("id");                
    }
       
    var op = "commenttweet";    
    $.ajax({
        type: "post",
        url: "../controller/ctweet.php",
        data: "op="+op+"&tweet="+tweet+"&comment="+comment,
    }).done(function(result) {                 
        var obj = JSON.parse(result);        
        if(obj.response === true){            
            obj['comment']=comment;
            html = formatTweetHTML(obj);
            
            if(btcomment.parents(".media").length === 1){//if it is the first comment
                btcomment.parent().parent().append(html);
            }else{
                $(html).insertAfter(btcomment.parent().parent().parent());
            }
            btcomment.parent().remove();
        } else{
            if(obj.response === false){
                alert("Você precisa acessar a sua conta para fazer comentários");
            }
        }          
    }).fail(function(){
        alert("Erro");
    });
 })

/*
 * FORMATS A SINGLE TWEET/MESSAGE WHEN IT IS FIRSTLY POSTED ON THE TIMELINE
 */
function formatTweetHTML(tweet){    
    return  '<div class="media">'+
                '<div class="media-left">'+
                    '<a href="mural.php?perfil=' + tweet.perfil+ '">'+
                        '<span class="glyphicon glyphicon-user" aria-hidden="true" style="font-size: 4em;"></span>'+
                    '</a>'+
                '</div>'+
                '<div class="media-body">'+
                    '<h5 class="media-heading""><a href="mural.php?perfil=' + tweet.perfil+ '"><span>'+tweet.name+'</span></a><span class="text-muted">'+ diffBetweenNow(tweet.date_time)+'</span></h5>'+
                    tweet.comment+
                    '<div class="likebox">'+
                        '<a href="javascript:void(0)" role="button" class="greenlink like" id="'+tweet.tweet+'">'+
                        '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Clique se gostou </a>'+
                    '</div>'+
                    '<div class="input-group" >'+
                        '<textarea name="tweet-'+tweet.tweet+'" class="form-control custom-control txtcomment"></textarea>'+
                        '<span class="input-group-addon btn btn-primary btcomment">Comentar</span>'+
                    '</div>'+
                '</div>'+
            '</div>';
}

/*
 * RETURNS THE CURRENT UTC STRING
 */
function getTimezoneString(){
    var d = new Date();
    return d.toUTCString();
}

/**
 * LOADS THE TIMELINE/TWEETS OF THE CURRENT MURAL
 * @param {type} param
 */
$( document ).ready(function() {
    var op = "loadtweets";
    var perfil = $("input#perfil");    
    
    $.ajax({
        type: "post",
        url: "../controller/ctweet.php",
        data: "op="+op+"&perfil="+perfil.val(),
    }).done(function(result) {             
        var obj = JSON.parse(result);                
        if(obj.response === true){            
            html = formatTweetHTML2(obj.tweets);
            $(html).insertAfter(".page-header");
        }             
    }).fail(function(){
        alert("Erro");
    });
});

/*
 * FORMATS A LIST OF TWEET/MESSAGE WHEN THE PAGE IS LOADED
 */
function formatTweetHTML2(tweets){
    var html = "";
    for(i in tweets){
        html += '<div class="media">'+
                    '<div class="media-left">'+
                        '<a href="mural.php?perfil=' + tweets[i].perfil+ '">'+
                            '<span class="glyphicon glyphicon-user" aria-hidden="true" style="font-size: 4em;"></span>'+
                        '</a>'+
                    '</div>'+
                    '<div class="media-body">'+
                        '<h5 class="media-heading""><a href="mural.php?perfil=' + tweets[i].perfil+ '"><span>'+$("#fullname").text()+'</span></a><span class="text-muted"> '+ diffBetweenNow(tweets[i].date_time)+'</span></h5>'+tweets[i].comment+
                        '<div class="likebox">';                                
                                if(tweets[i].like === true ){
                                    html+='<a href="javascript:void(0)" role="button" class="greenlink unlike" id="'+tweets[i].tweet+'">Você gostou disso. (Clique se não gostou)</a>';
                                }else{
                                    html+='<a href="javascript:void(0)" role="button" class="greenlink like" id="'+tweets[i].tweet+'"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Clique se gostou </a>';
                                }
                html += '</div>';
                        html += formatCommentsHTML(tweets[i]);                    
        html +=     '</div>'+
                '</div>';
    }
    return html;
}

/**
 * FORMATS THE COMMENTS AND "SUBCOMMENTS" OF A TWEET
 * @param {type} tweet
 * @returns {html|String|formatTweetHTML2.html}
 */
function formatCommentsHTML(tweet){
    countTweets = tweet.comments.length;    
    html = "";
    if(countTweets === 0){
        html = '<div class="input-group" >'+
                    '<textarea name="tweet-'+tweet.tweet+'" class="form-control custom-control txtcomment"></textarea>'+
                    '<span class="input-group-addon btn btn-primary btcomment">Comentar</span>'+
                '</div>';
    }else{
        for(var comment=0; comment < countTweets; comment++){                
            html += '<div class="media">'+
                        '<div class="media-left">'+
                            '<a href="mural.php?perfil=' + tweet.comments[comment].perfil+ '">'+
                                '<span class="glyphicon glyphicon-user" aria-hidden="true" style="font-size: 4em;"></span>'+
                            '</a>'+
                        '</div>'+
                        '<div class="media-body">'+
                            '<h5 class="media-heading""><a href="mural.php?perfil=' + tweet.comments[comment].perfil+ '"><span>'+tweet.comments[comment].name+'</span></a><span class="text-muted"> '+ diffBetweenNow(tweet.comments[comment].date_time)+'</span></h5>'+ tweet.comments[comment].comment+
                            '<div class="likebox">';                                
                                if(tweet.comments[comment].like === true ){
                                    html+='<a href="javascript:void(0)" role="button" class="greenlink unlike" id="'+tweet.comments[comment].tweet+'">Você gostou disso. (Clique se não gostou)</a>';
                                }else{
                                    html+='<a href="javascript:void(0)" role="button" class="greenlink like" id="'+tweet.comments[comment].tweet+'"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Clique se gostou </a>';
                                }
                     html += '</div>';                                                        
                if(comment === countTweets - 1){
                    html+=  '<div class="input-group" >'+
                                '<textarea name="tweet-'+tweet.comments[comment].tweet+'" class="form-control custom-control txtcomment"></textarea>'+
                                '<span class="input-group-addon btn btn-primary btcomment">Comentar</span>'+
                            '</div>';
                }
            html +=     '</div>'+
                    '</div>';
        }            
    }        
    return html;
}

/**
 * USER FRIENDLY FUNCTION TO DISPLAY THE TIME ELAPSED BETWEEN NOW AND A SPECIFIC TIME
 * @param {type} date within the ISO 8601 and UTC timezone
 * @returns {String}
 */
function diffBetweenNow(date) {
    var diff = "";          
    if(new Date(date).getTime()){
        var old = new Date(date);
        var mili = old.getTime();//UTC TIME        
        var d = new Date();
        var now = d.getTime();//UTC TIME
        var diffseconds = Math.floor((now - mili)/1000);
        diff = " Há ";
        if(diffseconds > 604800){
            function addZero(number)
            {
                return (number < 10) ? '0' + number : ""+number;
            }
            diff = [addZero(old.getDate()), addZero(old.getMonth()+1), old.getFullYear()].join('/');
        }else{
           if(diffseconds > 86400){
               diff += Math.floor(diffseconds / 86400)+" dia(s)";
           }else{
               if(diffseconds > 3600){               
                   diff += Math.floor(diffseconds / 3600)+" hora(s)";
               }else{
                   if(diffseconds > 60){
                       diff += Math.floor(diffseconds/60)+" minuto(s)";
                   }else{
                        diff += "alguns segundos";
                   }
               }
           }
        }
    }
    return diff;
}