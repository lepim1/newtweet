<?php
$currentdirectory = dirname(__FILE__);
include_once dirname($currentdirectory).'/model/dao/logindao.php';
include_once dirname($currentdirectory).'/model/datamodel/mlogin.php';

/**
 * CLASS TO VALIDATE USERNAMES. IT GET A EMAIL PASSED BY THE POST METHOD AND RETURNS A JSON OBJECT THAT INDICATES ITS EXISTENCE OR NOT ON THE DATABASE
 *
 * @author leonardopimentelferreira
 */
class CheckUsername{
    function __construct() {
        if(isset($_POST)){
            $logindao = new LoginDAOMySQL();
            $login = new Login();                
            $login->setUsername($_POST['username']);
            $result = $logindao->siginByUsername($login);
            if($result->rowCount() == 1){//email has been found on the database
                $arr = array("result"=>false);
                    echo json_encode($arr);
                    exit;
                }
            }        
        $arr = array("result"=>true);
        echo json_encode($arr);
        exit;
    }
}
$checkusername= new CheckUsername();
?>