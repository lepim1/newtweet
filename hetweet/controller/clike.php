<?php
$currentdirectory = dirname(__FILE__);
include_once dirname($currentdirectory).'/useful/session.php';
include_once dirname($currentdirectory).'/useful/functions.php';
include_once dirname($currentdirectory).'/model/datamodel/mlike.php';
include_once dirname($currentdirectory).'/model/dao/perfildao.php';
include_once dirname($currentdirectory).'/model/datamodel/mnotification.php';
include_once dirname($currentdirectory).'/model/datamodel/mperfil.php';

/**
 * CONTROL CLASS FOR LIKES
 *
 * @author leonardopimentelferreira
 */
class CLike {
    /**
     * SELECTS THE OPERATION TO EXECUTE. EX.: LIKE OR UNLIKE
     */
    function __construct() {
        if(isset($_GET["op"])){
            switch($_GET["op"]){
                case "like":
                    $this->likeToggle($_GET["op"]);
                    break;
                case "unlike":
                    $this->likeToggle($_GET["op"]);
                    break;
            }
        }
    }        
    
    /**
     * LIKES OR DISLIKES
     * @param type $op
     */
    function likeToggle($op){   
        $response = array("response"=>false);        
        $session = new Session();
        $session->sessionStart();      
        $like = new Like();
        $like->setIdperfilwholikes($session->getVariableonsession("perfil"));
        $like->setIdtweet((int)$_GET["tweet"]);
        $perfildao = new PerfilDAOMySQL();
        if($op == "like"){            
            if($perfildao->likeTweet($like)->rowCount()==1){ 
                if($like->getIdperfilwholikes() != $_GET["mural"]){
                    $this->sendNotification($like);
                }
                $response = array("response"=>true); 
            }
        }else{
            if($op == "unlike"){                
                if($perfildao->unLikeTweet($like)->rowCount()==1){
                    $response = array("response"=>true); 
                }
            }
        }        
        echo json_encode($response);
        exit;        
    }           
               
   /**
    * SENDS A NOTIFICATION WHEN A USER LIKES A TWEET   
    * @return type
    */    
    function sendNotification(Like $like){
        $functions = new Functions();
        $perfildao = new PerfilDAOMySQL();
        $notification = new Notification();
        $perfilfrom = new Perfil();
        $perfilto = new Perfil();        
        $perfilfrom->setIdperfil($like->getIdperfilwholikes());
        $notification->setPerfilfrom($perfilfrom);
        $perfilto->setIdperfil($_GET["mural"]);
        $notification->setPerfilto($perfilto);
        $notification->setDatetime($functions->getCurrentServerDateTimeToMYSQL());
        $notification->setTypenotificationlike();
        $notification->setStatusnew();        
        return $perfildao->sendNotificationPerfil($notification);
    }
}
$clike = new CLike();
?>
