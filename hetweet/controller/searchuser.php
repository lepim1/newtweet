<?php
$currentdirectory = dirname(__FILE__);
include_once dirname($currentdirectory).'/model/dao/logindao.php';
include_once dirname($currentdirectory).'/model/dao/perfildao.php';
include_once dirname($currentdirectory).'/model/datamodel/mlogin.php';
include_once dirname($currentdirectory).'/model/datamodel/mperfil.php';
include_once dirname($currentdirectory).'/useful/functions.php';

/**
 * CLASS THAT SEARCHES FOR A USER BY EMAIL, NAME OR USERNAME
 *
 * @author leonardopimentelferreira
 */
class SearchUser {        
    private $users; 
    /**
     * CONTROLS THE EXECUTION OF THE SEARCH PROCESS 
     */
    function __construct() {
        //echo "entrou"; 
        $this->users = array();
        if(isset($_GET["user"]) and strlen($_GET["user"])>=3){                        
            if(!$this->searchForEmail() or count($this->users)===0){
                //echo "fez 1 busca"; 
                $this->searchForFullName();
                //echo "fez 2 busca";
                if(count($this->users)===0){
                    //echo "fez 3 busca";
                    $this->searchForUserName();                        
                }
            }
        }        
        header('Content-Type: application/json');
        echo json_encode($this->users);        
        exit;
    }
    
    /**
     * ADD THE USERS THAT HAVE BEEN FOUND TO AN ARRAY. MAX: 10 USERS
     * @param type $result
     */
    function addUser($result, $maxuser=10){ 
        if($result->rowCount()>0){
            $users2 = $result->fetchAll();
            foreach ($users2 as $row) {   
                if(count($this->users)==($maxuser)){
                    break;                
                }           
                $this->users[$row["he_id_perfil"]] = array($row["he_full_name"],$row["he_locallization"]);
            }
        }
    }
        
    /**
     * SEARCHES FOR A USER BY EMAIL
     * @return boolean
     */
    function searchForEmail(){
        //echo "aqui no email"; 
        $functions = new Functions();
        if($functions->validateEmail($_GET["user"])){   
            //echo "depois de validar o email"; 
            $logindao = new LoginDAOMySQL(); 
            //echo "depois de criar o dao"; 
            $result = $logindao->searchForEmail($_GET["user"]);             
            //echo "depois de pesquisar por email";
            $this->addUser($result);           
            //echo "depois de add os usuarios";
            return true;
        }
        return false;        
    }
    
    /*
     * SEARCHES FOR A USER BY NAME
     */
    function searchForFullName(){        
        $perfildao = new PerfilDAOMySQL();            
        $result = $perfildao->searchByName($_GET["user"]);   
        $this->addUser($result);                    
    }
    
    /*
     * SEARCHES FOR A USER BY USERNAME
     */
    function searchForUserName(){
        $logindao = new LoginDAOMySQL();            
        $result = $logindao->searchForUsername($_GET["user"]);
        $this->addUser($result);        
    }
    
}
$searchuser = new SearchUser();
?>
