<?php
$currentdirectory = dirname(__FILE__);
include_once dirname($currentdirectory).'/useful/session.php';
include_once dirname($currentdirectory).'/useful/functions.php';
include_once dirname($currentdirectory).'/model/datamodel/mfollowing.php';
include_once dirname($currentdirectory).'/model/dao/perfildao.php';
include_once dirname($currentdirectory).'/model/datamodel/mperfil.php';
include_once dirname($currentdirectory).'/model/datamodel/mnotification.php';

/**
 * CONTROL CLASS FOR FOLLOWING SOMEONE
 *
 * @author leonardopimentelferreira
 */
class CFollow {
    /**
     * SELECTS THE OPERATION TO EXECUTE. EX.: FOLLOW, UNFOLLOW
     */
    function __construct() {
        if(isset($_GET["op"])){
            switch($_GET["op"]){
                case "follow":
                    $this->followToggle($_GET["op"]);
                    break;
                case "unfollow":
                    $this->followToggle($_GET["op"]);
                    break;
            }
        }
    }        
    
    /**
     * FOLLOW OR UNFOLLOW
     * @param type $op
     */
    function followToggle($op){        
        $response = array("response"=>false);        
        if(isset($_GET["perfil"])){
            $session = new Session();
            $session->sessionStart();        
            $followedid = (int)$_GET["perfil"];
            $followerid = $session->getVariableonsession("perfil");        
            
            $following = new Following();
            $following->setIdperfilfollowed($followedid);
            $following->setIdperfilfollower($followerid);            
            $perfildao = new PerfilDAOMySQL();
            if($op == "follow"){
                if($perfildao->followPerfil($following)->rowCount()==1){   
                    $this->sendNotificationFollowing($following);
                    $response = array("response"=>true); 
                }
            }else{
                if($op == "unfollow"){
                    if($perfildao->unFollowPerfil($following)->rowCount()==1){
                        $response = array("response"=>true); 
                    }
                }
            }
        }
        echo json_encode($response);
        exit;        
    }      
    
    /**
     * SENDS A NOTIFICATION WHEN A USER FOLLOWS ANOTHER ONE
     * @param Following $following
     * @return type
     */    
    function sendNotificationFollowing(Following $following){
        $functions = new Functions();
        $perfildao = new PerfilDAOMySQL();
        $notification = new Notification();
        $perfilfrom = new Perfil();
        $perfilto = new Perfil();        
        $perfilfrom->setIdperfil($following->getIdperfilfollower());
        $notification->setPerfilfrom($perfilfrom);
        $perfilto->setIdperfil($following->getIdperfilfollowed());
        $notification->setPerfilto($perfilto);
        $notification->setDatetime($functions->getCurrentServerDateTimeToMYSQL());
        $notification->setTypenotificationfollowing();
        $notification->setStatusnew();        
        return $perfildao->sendNotificationPerfil($notification);
    }
}
$cfollow = new CFollow();
?>
