<?php
$currentdirectory = dirname(__FILE__);
include_once dirname($currentdirectory).'/model/datamodel/mtweet.php';
include_once dirname($currentdirectory).'/model/datamodel/mperfil.php';
include_once dirname($currentdirectory).'/model/dao/perfildao.php';
include_once dirname($currentdirectory).'/model/dao/tweetdao.php';
include_once dirname($currentdirectory).'/useful/session.php';
include_once dirname($currentdirectory).'/useful/functions.php';


/**
 * CONTROL CLASS FOR TWEETS
 *
 * @author leonardopimentelferreira
 */
class CTweet {
    /**
     * SELECTS THE OPERATION TO EXECUTE. EX.: ADD NEW TWEET OR COMMENT
     */
    function __construct() {        
        if(isset($_POST["op"])){
            switch($_POST["op"]){
                case "postnewmessage":
                    $this->createNewTweet();
                    break;                
                case "commenttweet":
                    $this->commentTweet();
                    break; 
                case "loadtweets":
                    $this->loadTweet();
                    break;                 
            }
        }        
    }                    
    
    /*
     * CREATES A NEW TWEET
     */
    function createNewTweet(){
        $response = array("response" => false);
        if(isset($_POST["postmessage"])){
            $functions = new Functions();
            $session = new Session();
            $session->sessionStart(); 
            
            $msg = $functions->avoidXSS($_POST["postmessage"]);             
            
            $tweet = new Tweet();
            $tweet->setDatetime($functions->getCurrentServerDateTimeToMYSQL());
            $tweet->setTweet($msg);
            $tweet->setIdperfil($session->getVariableonsession("perfil"));
            
            $tweetdao = new TweetDAOMySQL();            
            $tweetdao->addNewTweet($tweet);
            $response["response"] = true;
            $response["tweet"] = $tweetdao->lastInsertId();
            $response["perfil"] = $tweet->getIdperfil();   
            $response["date_time"] = $tweet->getDatetime();
        }
        echo json_encode($response);
        exit;
    }

    /*
     * CREATES A COMMENT TO A TWEET CREATED PREVIOUSLY
     */
    function commentTweet(){                
        $response = array("response" => false);
        if(isset($_POST["comment"]) and isset($_POST["tweet"])){
            $session = new Session();
            $session->sessionStart(); 
            $perfilsession = $session->getVariableonsession("perfil");            
            if($perfilsession != NULL){                                            
                $functions = new Functions();
                $idtweet = (int)$_POST["tweet"];
                $comment = $functions->avoidXSS($_POST["comment"]);                         

                $tweet = new Tweet();
                $tweet->setDatetime($functions->getCurrentServerDateTimeToMYSQL());
                $tweet->setIdoriginal($idtweet);
                $tweet->setTweet($comment);                      
                $tweet->setIdperfil($session->getVariableonsession("perfil"));

                $tweetdao = new TweetDAOMySQL();            
                $tweetdao->addComment($tweet);
                $response["response"] = true;
                $response["tweet"] = $tweetdao->lastInsertId();
                $response["perfil"] = $tweet->getIdperfil();
                $response["date_time"] = $tweet->getDatetime();
                $response["name"] = $session->getVariableonsession("name");
            }
        }
        echo json_encode($response);
        exit;
    }
    
    /**
     * LOAD ALL THE TWEETS OF THE CURRENT MURAL/PERFIL THAT THE USER WANTS TO SEE
     */
    function loadTweet(){
        $tweets = array("response" => false);       
        if(isset($_POST["perfil"])){
            $functions = new Functions();
            $perfil = new Perfil();
            $perfil->setIdperfil($_POST["perfil"]);
            $session = new Session();
            $session->sessionStart();             
            $perfildao = new PerfilDAOMySQL();
            $perfilsession = $session->getVariableonsession("perfil");
            $likes = array();
            if($perfilsession != NULL){
                $statm = $perfildao->searchForLikes($perfilsession);
                $likes = $statm->fetchAll(PDO::FETCH_COLUMN);                        
            }
            $tweetdao = new TweetDAOMySQL();
            $statm = $tweetdao->searchForTweets($perfil);   //search for tweets
            if($statm->rowCount()>0){//if there is at least one tweet
                $tweets["response"] = true;
                $result = $statm->fetchAll();//get all tweets
                foreach ($result as $row){//for each tweet of the mural
                    $tweetsaux = array();
                    $tweetsaux['tweet'] = $row["he_id_tweet"];
                    $tweetsaux['perfil'] = $row["he_id_perfil"];                    
                    $tweetsaux['comment'] = $row["he_tweet"];                    
                    $tweetsaux['date_time'] = $functions->convertDateTimeToIso8601($row["he_date_time"]);                    
                    $tweetsaux['like']= in_array($row["he_id_tweet"],$likes);
                    $statm = $tweetdao->searchForComments($row["he_id_tweet"]);//if the tweet has comments
                    $comments = array();
                    
                    $result2 = $statm->fetchAll();//get all comments
                    foreach ($result2 as $row2){//for each comment
                        $comment = array();
                        $comment["tweet"] = $row2['he_id_tweet'];
                        $comment["perfil"] = $row2['he_id_perfil'];
                        $comment["date_time"] = $functions->convertDateTimeToIso8601($row2['he_date_time']);
                        $comment['comment'] = $row2["he_tweet"];
                        $comment["name"] = $row2['he_full_name'];
                        $comment['like']= in_array($row2["he_id_tweet"],$likes);
                        $comments[] = $comment;
                    }
                    $tweetsaux["comments"]=$comments;
                    $tweets["tweets"][] = $tweetsaux;                    
                }                
            }
        }
        echo json_encode($tweets);
        exit;
    }   
}

$ctweet = new CTweet();
?>
