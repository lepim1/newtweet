<?php

/**
 * CLASS TO VALIDATE GOOGLE RECAPTCHA VERSION 2
 * IT MAKES A REQUEST TO THE WEBSITE AND RETURNS A BOOLEAN (SUCCESS OR FAIL)
 * @author leonardopimentelferreira
 */
class CheckCaptcha{
    
    function __construct() {                                                    
        
    }
    
    function validateCaptchaByGoogle(){
        if(isset($_POST['g-recaptcha-response'])){            
            $url='https://www.google.com/recaptcha/api/siteverify?secret=6Ld0WwATAAAAAOyRnkQgtRkC91ahF2-GOrR44Phg&response='.$_POST["g-recaptcha-response"]."&remoteip=".$_SERVER['REMOTE_ADDR'];
            //echo $url."<br>";
            $r = file_get_contents($url);
            if(isset($r)){
                $arr = json_decode($r, true);            
                if($arr["success"] == true){
                    //echo "Google Validated!";
                    return true;
                }
            }
        }
        return false;
    }
}
?>

