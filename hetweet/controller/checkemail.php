<?php
    include_once dirname(dirname(__FILE__)).'/model/dao/logindao.php';
    include_once dirname(dirname(__FILE__)).'/model/datamodel/mlogin.php';
    
    /**
     * CLASS TO VALIDATE EMAILS. IT GET A EMAIL PASSED BY THE POST METHOD AND RETURNS A JSON OBJECT THAT INDICATES ITS EXISTENCE OR NOT ON THE DATABASE
     *
     * @author leonardopimentelferreira
     */
    class CheckEmail{
        function __construct() {
            if(isset($_POST)){
                $logindao = new LoginDAOMySQL();
                $login = new Login();                
                $login->setEmail($_POST['email']);
                $result = $logindao->siginByEmail($login);
                if($result->rowCount() == 1){//email has been found on the database
                    $arr = array("result"=>false);
                    echo json_encode($arr);
                    exit;
                }
            }        
            $arr = array("result"=>true);
            echo json_encode($arr);
            exit;
        }         
    }
    
    $checkemail = new CheckEmail();
?>

