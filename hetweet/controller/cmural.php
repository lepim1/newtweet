<?php    
    $currentdirectory = dirname(__FILE__);
    include_once dirname($currentdirectory).'/model/dao/perfildao.php';
    include_once dirname($currentdirectory).'/model/datamodel/mperfil.php';
    include_once dirname($currentdirectory).'/model/datamodel/mlocation.php';
    include_once dirname($currentdirectory).'/model/datamodel/mnotification.php';    
    include_once dirname($currentdirectory).'/model/dao/locationdao.php';
    include_once dirname($currentdirectory).'/useful/functions.php';

    /**
    * CONTROLLER CLASS THAT GETS SOME INFO OF THE MURAL PAGE SUCH AS THE SIDE BAR
    *
    * @author leonardopimentelferreira
    */
    class CMural{ 
        function getPerfilById(Perfil $perfil){                         
            $perfildao = new PerfilDAOMySQL();
            $result = $perfildao->getPerfilById($perfil); 
            if($result->rowCount() == 1){
                $row = $result->fetch();
                $perfil->setFullname($row['he_full_name']);
                $perfil->setRelationship($row['he_relationship']);
                $perfil->setPathphotoserver($row['he_photo_server_path']);
                $perfil->setIdlogin($row['he_id_login']);
                $perfil->setWhoiam($row['he_who_i_am']);
                $perfil->setLocal($row['he_id_locallization']);
            }else{
                $perfil->setIdperfil(NULL);
            }
            return $perfil;         
        }        
        function isFollowing($perfilsession, $perfilmural){
            $perfildao = new PerfilDAOMySQL();
            $statm = $perfildao->isFollowing($perfilsession, $perfilmural);            
            return $statm->fetchColumn()==1;
        }
        function getLocation(Perfil $perfil){
            $locationdao = new LocationDAOMySQL();
            $location = new Location();    
            $location->setIdlocation($perfil->getLocal());
            $result = $locationdao->searchById($location);
            if($result->rowCount() == 1){
                $row = $result->fetch();
                $location->setIdlocation($row["he_id_locallization"]);
                $location->setLocation($row["he_locallization"]);
            }
            return $location;
        }        
        function getCountFollowersAndFollowings(Perfil $perfil) { 
            $perfildao = new PerfilDAOMySQL();                        
            $statm = $perfildao->getCountFollowers($perfil);
            $followers = $statm->fetchColumn();
            $statm = $perfildao->getCountFollowings($perfil);            
            $followings = $statm->fetchColumn();            
            $result = array($followers, $followings);
            return $result;         
        }
    }
        
?>
