<?php
$currentdirectory = dirname(__FILE__);
include_once dirname($currentdirectory).'/model/datamodel/mlogin.php';
include_once dirname($currentdirectory).'/model/datamodel/mlocation.php';
include_once dirname($currentdirectory).'/model/datamodel/mperfil.php';
include_once dirname($currentdirectory).'/model/dao/logindao.php';
include_once dirname($currentdirectory).'/model/dao/locationdao.php';
include_once dirname($currentdirectory).'/model/dao/perfildao.php';
include_once dirname($currentdirectory).'/useful/functions.php';
include_once dirname($currentdirectory).'/useful/session.php';
include_once $currentdirectory.'/checkcaptcha.php';

/**
 * Description of clogin
 *
 * @author leonardopimentelferreira
 */
class CLogin {
    /**
     * SELECTS THE OPERATION TO EXECUTE. EX.: SIGN IN, SIGN UP, SIGN OUT
     */
    function __construct() {
        if(isset($_GET["op"])){
            switch($_GET["op"]){                
                case "signout":
                    self::signOut();
                    break;                                              
            }
        }else{
            if(isset($_POST["op"])){
                switch($_POST["op"]){
                    case "forgot":
                        $this->forgotPassword();
                        break;
                    case "signin":
                        self::signIn();
                        break;
                    case "signup":
                        $this->signUp();
                        break;  
                    case "forgotreset":
                        $this->forgotResetPassword();
                        break;
                }
            }
        }
    }
    
    /**
     *DENIES A LOG IN ATTEMPTIVE OR
     *SIGNS IN THE USER AND CREATES A SESSION FOR HIM
     */
    function signIn(){
        $arr = array("response" => false);                    
        if(isset($_POST["user"]) and isset($_POST["password"])){
            $functions = new Functions();
            $username = $functions->avoidXSS($_POST["user"]);
            $password = $_POST["password"];
            $logindao = new LoginDAOMySQL();
            $functions = new Functions();
            $login = new Login();

            if ($functions->validateEmail($username)) {
                $login->setEmail($username);
                $result = $logindao->siginByEmail($login);
            }else{
                $login->setUsername($username);
                $result = $logindao->siginByUsername($login);
            }
            if($result->rowCount() == 1){
                $row = $result->fetch();
                if($functions->decryptPassword($password, $row['he_pass'])){                                        
                    $session = new Session();
                    $session->setVariableonsession('login', $row['he_id_login']);
                    $perfildao = new PerfilDAOMySQL();
                    $perfil = new Perfil();
                    $perfil->setIdlogin($row['he_id_login']);
                    $row = $perfildao->getIdPerfilNameByIdLogin($perfil)->fetch();
                    $session->setVariableonsession('perfil', $row['he_id_perfil']);
                    $session->setVariableonsession('name', $row['he_full_name']);
                    $arr["response"] = true;
                }               
            }
        }        
        echo json_encode($arr);
        exit;
    }
    
    /**
     *LOG OFF FUNCTION
     */
    function signOut(){
        $session = new Session();
        $session->sessionEnd();        
        header("Location: ../view/login.php");
        exit;
    }
    
    /**
     * SIGN UP METHOD
     */
    function signUp(){  
        $arr = array("response"=> "error");//INVALID REQUEST
        if(isset($_POST)){            
            if($this->validateCaptcha()){     
                $login = new Login();
                $functions = new Functions();          
                $location = new Location();
                $perfil = new Perfil();
                
                //GET AND HANDLE DATA
                $email = $_POST["email"];
                $password = $functions->encryptPassword($_POST["password"]);
                $fullname = $functions->avoidXSS($_POST["fullname"]);
                $local = $functions->avoidXSS($_POST["local"]);
                $relationship = $functions->avoidXSS($_POST["relationship"]);
                $whoiam = $functions->avoidXSS($_POST["whoiam"]);
                $username = $functions->avoidXSS($_POST["username"]);                                               
                
                //SET AND INSERT LOGIN
                $login->setEmail($email);
                $login->setPass($password);
                $login->setAccountstatusactive();
                $login->setUsername($username);
                $login->setStartdate($functions->getCurrentServerDateTimeToMYSQL());                                
                $this->insertLogin($login);
                
                //SET AND INSERT LOCATION
                $location->setLocation($local);                
                $this->insertLocation($location);
                
                //SET AND INSERT PERFIL
                $perfil->setFullname($fullname);
                $perfil->setRelationship($relationship);
                $perfil->setWhoiam($whoiam);
                $perfil->setIdlogin($login->getIdlogin());
                $perfil->setWhoiam($whoiam);    
                $perfil->setLocal($location->getIdlocation());
                $this->insertPerfil($perfil);                
                $arr = array("response"=> "ok");//USER REGISTERED               
            }else{                
                $arr = array("response"=> "ic");//INVALID CAPTCHA
            }
        }         
        echo json_encode($arr);
        exit;
    }
    
    /**
     * VALIDATES RECAPTCHA
     * @return type
     */
    function validateCaptcha(){
        $checkecaptcha = new CheckCaptcha();
        $r = $checkecaptcha->validateCaptchaByGoogle();            
        return $r;
    } 
    
    /**
     * INSERTS DATA INTO HE_LOGIN TABLE
     * @return type
     */
    function insertLogin(Login $login){
        $logindao = new LoginDAOMySQL();
        $result = $logindao->add($login);        
        $login->setIdlogin($logindao->lastInsertId());
        return $result;
    } 
    
    /**
     * INSERTS DATA INTO HE_LOGIN TABLE
     * @return type
     */
    function insertLocation(Location $location){
        $locationdao = new LocationDAOMySQL();
        $result = $locationdao->searchByLocalization($location);
        if($result->rowCount() == 0){            
            $result = $locationdao->add($location);             
            $location->setIdlocation($locationdao->lastInsertId());
        }else{                    
            $row = $result->fetch();
            $location->setIdlocation($row['he_id_locallization']);
        }
        return $result;
    }
    
    /**
     * INSERTS PERFIL
     * @return type
     */
    function insertPerfil(Perfil $perfil){
        $perfildao = new PerfilDAOMySQL();        
        $result = $perfildao->add($perfil);        
        $perfil->setIdperfil($perfildao->lastInsertId());
        return $result;
    }
    
    /**
     * CREATES A TOKEN AND SEND AN EMAIL TO THE USER
     * @return type
     */
    function forgotPassword(){
        $arr = array('response' => false);
        if(isset($_POST)){
            $logindao = new LoginDAOMySQL();            
            $login = new Login();           
            $functions = new Functions();           
            $login->setEmail($functions->avoidXSS($_POST['email']));
            $result = $logindao->siginByEmail($login);
            if($result->rowCount() == 1){//email has been found on the database
                $row = $result->fetch();
                $login->setIdlogin($row['he_id_login']);
                $login->setTokenresetpass($functions->generate512bitstoken());
                $login->setTokenexpiration($functions->getExpirationSevenDaysFromNow());
                $logindao->updateToken($login);         
                if($functions->sendEmailResetPassword($login)){
                    $arr['response'] = true;               
                }                                
            }                        
        }      
        echo json_encode($arr);
        exit;
    }
    
    /**
     * THE DATE OF EXPIRATION SHOULD BE GREATER THAN NOW AND TOKEN MUST EXIST
     * @param type $token
     * @return boolean
     */
    function istokenvalid($token){        
        $logindao = new LoginDAOMySQL();
        $function = new Functions();
        $login = new Login();        
        $login->setTokenresetpass($function->avoidXSS($token));
        $r = $logindao->searchValidTokenByLogin($login);        
        if($r->rowCount() == 1){//found it            
            return true;
        }else{                        
            return false;
        }
    }
    
    /**
     * THIS FUNCTION GETS A TOKEN FROM THE POST METHOD, VALIDATES AND THEN RESET IT ON THE DATABASE
     */
    function forgotResetPassword(){
        if(isset($_POST['tokenform'])){
            $token = $_POST['tokenform'];
            if($this->istokenvalid($token)){                
                $logindao = new LoginDAOMySQL();
                $login = new Login();       
                $functions = new Functions();                  
                $login->setTokenresetpass($token);
                $login->setPass($functions->encryptPassword($_POST['password']));
                $logindao->updatePassNullToken($login);
                header("Location: ../view/login.php?msg=passok");
                exit;
            }            
        }        
    }
}
$clogin = new CLogin();
?>
