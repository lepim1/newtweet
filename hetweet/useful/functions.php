<?php
require('../../vendor/autoload.php');

/**
 * THIS FILE CONTAINS USEFULL FUNCTIONS TO BE USED THROUGHOUT THE PROJECT
 *
 * @author leonardopimentelferreira
 */
class Functions {
    /**
     * ENCRYPTS A STRING USING BCRYPT ALGHORITHM
     * @param type $password STRING TO BE ENCRYPTED
     * @return string encrypted
     */
    public function encryptPassword($password){
        return password_hash($password, PASSWORD_DEFAULT);
    }
    /**
     * DENRYPTS A BCRYPTED STRING
     * @param string $password
     * @return boolean IF TRUE, THE PASSWORD IS VALID
     */
    public function decryptPassword($password, $hash){
        return password_verify ($password, $hash);
    }
    
    /**
     * VALIDATES AN EMAIL
     * @param type $email
     * @return boolean
     */
    public function validateEmail($email){        
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
    
    /**
     * RETURNS THE NEW YORK CURRENT TIME TO BE REGISTERED ON THE DATABASE
     * @return type
     */
    public function getCurrentServerDateTimeToMYSQL(){
        $this->setDefaultTimeZone();
        return date('Y-m-d H:i:s', time());
    }
    
    /**
     * ANTI-XSS FUNCTION
     * @param type $string
     * @return type
     */
    public function avoidXSS($string){
        return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
    }
    
    /**
     * RETURNS A 512 TOKEN
     * @return type
     */
    public function generate512bitstoken(){
        return hash('sha512', uniqid().'he:tweets2015');
    }      
    
    /**
     * CREATES AN EXPIRATION DATETIME OF 7 DAYS FROM NOW
     * @return type
     */
    public function getExpirationSevenDaysFromNow(){       
        $this->setDefaultTimeZone();
        $date = strtotime("+7 day");
        return date('Y-m-d H:i:s', $date);
    }
    
    /**
     * SENDS AN EMAIL
     * @param type $email
     * @param type $subject
     * @param type $message
     * @return type
     */
    public function sendEmail($email, $subject, $message, $from="" ){
        //--------------production-------------
        // In case any of our lines are larger than 70 characters, we should use wordwrap()
        //$message2 = wordwrap($message, 70, "\r\n");
        // Send        
        //return mail($email, $subject, $message2, $from);       
        
        //--------------operation-------------
         $sendgrid = new SendGrid('app33870533@heroku.com', 't7tkxtgg');        
         $msg = new SendGrid\Email();
         $msg->addTo($email)->
                   setFrom($from)->
                   setSubject($subject)->
                   setHtml($message);
         return $sendgrid->send($msg);
         
    }
    
    /**
     * DEBUGS ANY VARIABLE
     * @param type $var
     */
    public function debugToFile($var){
        $myfile = fopen("debug.txt", "w") or die("Unable to open file!");    
        ob_start();
        var_dump(func_get_args());
        $result = ob_get_clean();
        fwrite($myfile, "Var dump".$result);    
        fwrite($myfile, "\nVar export".var_export(func_get_args($var), true));
        fclose($myfile); 
    }    

    /**
     * SENDS AN EMAIL TO RESET PASSWORD. IT ASSUMES THAT LOGIN HAS ALREADY GENERATED A TOKEN
     * @param Login $login
     * @return type
     */
    public function sendEmailResetPassword(Login $login){                    
        $website = "https://sheltered-springs-9591.herokuapp.com/hetweet/view/forgot.php?token=".$login->getTokenresetpass();
        $message = "Se você solicitou uma nova senha para acessar o seu blog na HE Tweets, clique no link a seguir: <a href=\"$website\">Alterar senha</a>";
        $subject = "HE Tweets: Solicitação de alteração de senha";
        $from = "hetweets1@gmail.com";
        $r = $this->sendEmail($login->getEmail(), $subject, $message, $from);
        return $r;
    }
    
    public function setDefaultTimeZone() {
        date_default_timezone_set('UTC');
    }
    
    public function convertDateTimeToIso8601($datetime){
        $this->setDefaultTimeZone();
        $d = new DateTime($datetime);
        return $d->format(DateTime::ISO8601);
    }
       
}
