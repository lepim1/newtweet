<?php

/**
 * CLASS TO MANAGE SESSIONS
 *
 * @author leonardopimentelferreira
 */
class Session {            
    public function sessionStart(){
        if(!isset($_SESSION)) {
            // session isn't started
            session_start();
        }
    }
    
    /**
     * GETS AN KEY (VARIABLE) AND A VALUE TO BE SET ON THE SESSION
     * @param type $variable
     * @return type
     */
    public function setVariableonsession($variable, $value){
        self::sessionStart();
        $_SESSION[(string)$variable] = $value;
    }
    
    /**
     * GETS AN KEY (VARIABLE) AND RETURNS THE VALUE OF IT STORED ON THE SESSION
     * @param type $variable
     * @return type
     */
    public function getVariableonsession($variable){        
        self::sessionStart();
        if(isset($_SESSION[(string)$variable])){
            return $_SESSION[(string)$variable];
        }else{
            return NULL;
        }
    }
    
    public function sessionEnd(){
        self::sessionStart();
        // REMOVE ALL SESSION VARIABLES
        session_unset(); 
        // DESTROY THE SESSION
        session_destroy();        
    }
    
    /**
     * VERIFIES IF A USER IS LOGGED IN THROUGH LOOKING FOR THE LOGIN KEY STORED ON THE SESSION
     * @return boolean
     */
    public function verifiesLogin(){
        self::sessionStart();
        if(isset($_SESSION['login'])){
            return true;
        }else{
            return false;
        }
    }
}
?>
