<?php

/**
 * Description of perfil
 *
 * @author leonardopimentelferreira
 */
class Tweet {
    private $idperfil;
    private $idtweet;
    private $tweet;
    private $datetime;
    private $idoriginal;
    
    function getIdperfil() {
        return $this->idperfil;
    }

    function getIdtweet() {
        return $this->idtweet;
    }

    function getTweet() {
        return $this->tweet;
    }

    function getDatetime() {
        return $this->datetime;
    }

    function getIdoriginal() {
        return $this->idoriginal;
    }

    function setIdperfil($idperfil) {
        $this->idperfil = $idperfil;
    }

    function setIdtweet($idtweet) {
        $this->idtweet = $idtweet;
    }

    function setTweet($tweet) {
        $this->tweet = $tweet;
    }

    function setDatetime($datetime) {
        $this->datetime = $datetime;
    }

    function setIdoriginal($idoriginal) {
        $this->idoriginal = $idoriginal;
    }
}
?>