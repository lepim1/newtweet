<?php

/**
 * MODEL CLASS FOR LOCATION: IT IS BASICALLY THE CONCATENATION OF CITY, STATE, AND COUNTRY
 * 
 * @author leonardopimentelferreira
 */
class Location{
    private $idlocation;
    private $location;
    
    function getIdlocation() {
        return $this->idlocation;
    }

    function getLocation() {
        return $this->location;
    }
    
    function setIdlocation($idlocation) {
        $this->idlocation = $idlocation;
    }

    function setLocation($location) {
        $this->location = $location;
    }
}
?>
