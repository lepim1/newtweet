<?php

/**
 * Description of like
 *
 * @author leonardopimentelferreira
 */
class Like {
    private $idperfilwholikes;
    private $idtweet;
    
    function getIdperfilwholikes() {
        return $this->idperfilwholikes;
    }

    function getIdtweet() {
        return $this->idtweet;
    }

    function setIdperfilwholikes($idperfilwholikes) {
        $this->idperfilwholikes = $idperfilwholikes;
    }

    function setIdtweet($idtweet) {
        $this->idtweet = $idtweet;
    }
    
}
?>