<?php

/**
 * MODEL CLASS FOR LOGIN
 * 
 * @author leonardopimentelferreira
 */
class Login{
    private $idlogin;
    private $email;
    private $username;
    private $pass;
    private $startdate;
    private $tokenresetpass;
    private $tokenexpiration;
    private $accountstatus; //"ACTIVE", "INCATIVE" or "BLOCKED"
    
    public function getIdlogin() {
        return $this->idlogin;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPass() {
        return $this->pass;
    }

    public function getStartdate() {
        return $this->startdate;
    }

    public function getTokenresetpass() {
        return $this->tokenresetpass;
    }

    public function getAccountstatus() {
        return $this->accountstatus;
    }

    function getTokenexpiration() {
        return $this->tokenexpiration;
    }
       
    public function setIdlogin($idlogin) {
        $this->idlogin = $idlogin;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setPass($pass) {
        $this->pass = $pass;
    }

    public function setStartdate($startdate) {
        $this->startdate = $startdate;
    }

    public function setTokenresetpass($tokenresetpass) {
        $this->tokenresetpass = $tokenresetpass;
    }
    
    function setTokenexpiration($tokenexpiration) {
        $this->tokenexpiration = $tokenexpiration;
    }
        
    public function setAccountstatus($accountstatus) {
        $this->accountstatus = $accountstatus;
    }
    
    public function setAccountstatusactive() {
        $this->accountstatus = "ACTIVE";
    }
    
    public function setAccountstatusinactive() {
        $this->accountstatus = "INACTIVE";
    }
    
    public function setAccountstatusblocked() {
        $this->accountstatus = "BLOCKED";
    }
    
}
?>
