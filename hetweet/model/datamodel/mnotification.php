<?php

/**
 * Description of Notification
 *
 * @author leonardopimentelferreira
 */
class Notification implements JsonSerializable {
    private $perfilfrom;
    private $perfilto;
    private $idnotification;
    private $typenotification;
    private $datetime;
    private $status;
    
    function getPerfilfrom() {
        return $this->perfilfrom;
    }

    function getperfilto() {
        return $this->perfilto;
    }

    function getIdnotification() {
        return $this->idnotification;
    }

    function getTypenotification() {
        return $this->typenotification;
    }

    function getDatetime() {
        return $this->datetime;
    }

    function getStatus() {
        return $this->status;
    }
    
    function setPerfilfrom(Perfil $perfilfrom) {
        $this->perfilfrom = $perfilfrom;
    }

    function setPerfilto($perfilto) {
        $this->perfilto = $perfilto;
    }

    function setIdnotification($idnotification) {
        $this->idnotification = $idnotification;
    }

    function setTypenotification($typenotification) {
        $this->typenotification = $typenotification;
    }
    
    function setTypenotificationfollowing(){
        $this->typenotification = "FOLLOWING";
    }        
    
    function setTypenotificationlike(){
        $this->typenotification = "LIKE";
    }   
    
    function setDatetime($datetime) {
        $this->datetime = $datetime;
    }

    function setStatus($status) {
        $this->status = $status;
    }
    
    function setStatusnew() {
        $this->status = "NEW";
    }

    public function jsonSerialize() {
        return [
            'idperfilfrom' => $this->perfilfrom->getIdperfil(),
            'fullname' => $this->perfilfrom->getFullname(),
            'idperfilto' => $this->perfilto->getIdperfil(),
            'datetime' => $this->datetime,
            'idnotification' => $this->idnotification,
            'typenotification' => $this->typenotification,            
            'status' => $this->status
        ];
    }
}
?>
