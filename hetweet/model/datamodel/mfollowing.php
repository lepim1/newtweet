<?php

/**
 * Description of following
 *
 * @author leonardopimentelferreira
 */
class Following {
    private $idperfilfollower;
    private $idperfilfollowed;
    
    function getIdperfilfollower() {
        return $this->idperfilfollower;
    }

    function getIdperfilfollowed() {
        return $this->idperfilfollowed;
    }
    
    function setIdperfilfollower($idperfilfollower) {
        $this->idperfilfollower = $idperfilfollower;
    }

    function setIdperfilfollowed($idperfilfollowed) {
        $this->idperfilfollowed = $idperfilfollowed;
    }
}
?>