<?php

/**
 * Description of perfil
 *
 * @author leonardopimentelferreira
 */
class Perfil {
    private $idperfil;
    private $fullname;
    private $relationship;
    private $pathphotoserver;
    private $idlogin;
    private $whoiam;
    private $local;
    
    function getIdperfil() {
        return $this->idperfil;
    }

    function getFullname() {
        return $this->fullname;
    }

    function getRelationship() {
        return $this->relationship;
    }

    function getPathphotoserver() {
        return $this->pathphotoserver;
    }

    function getIdlogin() {
        return $this->idlogin;
    }

    function getWhoiam() {
        return $this->whoiam;
    }

    function getLocal() {
        return $this->local;
    }

    function setIdperfil($idperfil) {
        $this->idperfil = $idperfil;
    }

    function setFullname($fullname) {
        $this->fullname = $fullname;
    }

    function setRelationship($relationship) {
        $this->relationship = $relationship;
    }

    function setPathphotoserver($pathphotoserver) {
        $this->pathphotoserver = $pathphotoserver;
    }

    function setIdlogin($idlogin) {
        $this->idlogin = $idlogin;
    }

    function setWhoiam($whoiam) {
        $this->whoiam = $whoiam;
    }

    function setLocal($local) {
        $this->local = $local;
    }   
}
?>