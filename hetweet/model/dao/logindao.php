<?php

include_once 'mysqlpdo.php';

/**
 * CLASS TO MANIPULATE THE DATABASE WITH REGARDING TO LOGIN OPERATIONS
 *
 * @author leonardopimentelferreira
 */
class LoginDAOMySQL extends MySQLPDO {

    public function __construct($driver_options = array(PDO::ATTR_PERSISTENT => false, PDO::MYSQL_ATTR_INIT_COMMAND =>  'SET NAMES utf8')) {
        parent::__construct($driver_options);
    }

    public function add(Login $login) {
        return $this->query("INSERT INTO he_login (he_email, he_user_name, he_pass, he_start_date, he_account_status) VALUES (?,?,?,?,?)", 
                $login->getEmail(), 
                $login->getUsername(), 
                $login->getPass(),
                $login->getStartdate(),
                $login->getAccountstatus());
    }
    
    public function siginByEmail(Login $login) {        
        return $this->query("SELECT * FROM he_login WHERE he_email = ?", $login->getEmail());
    }
    
    public function siginByUsername(Login $login){
        return $this->query("SELECT * FROM he_login WHERE he_user_name = ?", $login->getUsername());
    }
    
    public function updateToken(Login $login){
        return $this->query("UPDATE he_login SET he_token_reset_pass = ?, he_token_expiration = ? WHERE he_id_login = ?", 
                $login->getTokenresetpass(), $login->getTokenexpiration(), $login->getIdlogin());
    }
    
    public function validateToken(Login $login){       
        return $this->query("UPDATE he_login SET he_token_reset_pass = NULL, he_token_expiration = NULL WHERE he_token_reset_pass = ?", 
                $login->getTokenresetpass());
    }
    
    public function searchValidTokenByLogin(Login $login){       
        return $this->query("SELECT * FROM he_login WHERE he_token_reset_pass = ? and he_token_expiration > now()", 
                $login->getTokenresetpass());
    }
    
    public function updatePassNullToken(Login $login){
        return $this->query("UPDATE he_login SET he_token_reset_pass = NULL, he_token_expiration = NULL, he_pass = ? WHERE he_token_reset_pass = ?", 
                $login->getPass(),
                $login->getTokenresetpass());
    }
    
    public function searchForEmail($email) {        
        $sql = "SELECT p.he_full_name, loc.he_locallization, p.he_id_perfil FROM "
                . "he_login login, he_perfil p, he_locallization loc WHERE "
                . "login.he_email like ? and "
                . "login.he_id_login = p.he_id_login and "
                . "p.he_id_locallization = loc.he_id_locallization LIMIT 10";    
        
        //PREPARED STATEMENT
        $stmt = $this->prepare($sql);        
        $stmt->bindValue(1, "%$email%", PDO::PARAM_STR);                
        $stmt->execute();
        return $stmt;
    }
    
    public function searchForUsername($username) {        
        $sql = "SELECT p.he_full_name, loc.he_locallization, p.he_id_perfil FROM "
                . "he_login login, he_perfil p, he_locallization loc WHERE "
                . "login.he_user_name like ? and "
                . "login.he_id_login = p.he_id_login and "
                . "p.he_id_locallization = loc.he_id_locallization LIMIT 10";    
        
        //PREPARED STATEMENT
        $stmt = $this->prepare($sql);        
        $stmt->bindValue(1, "%$username%", PDO::PARAM_STR);               
        $stmt->execute();
        return $stmt;
    }
    
}
?>

