<?php

include_once 'mysqlpdo.php';

/**
 * CLASS TO MANIPULATE THE DATABASE WITH REGARDING TO LOCATION OPERATIONS
 *
 * @author leonardopimentelferreira
 */
class LocationDAOMySQL extends MySQLPDO {

    public function __construct($driver_options = array(PDO::ATTR_PERSISTENT => true, PDO::MYSQL_ATTR_INIT_COMMAND =>  'SET NAMES utf8')) {
        parent::__construct($driver_options);
    }

    public function add(Location $location) {  
        
        return $this->query("INSERT INTO he_locallization (he_locallization) VALUES (?)", 
                $location->getLocation());
    }
    
    public function searchByLocalization(Location $location) {
        return $this->query("SELECT * FROM he_locallization where he_locallization = ?", 
                $location->getLocation());
    }
    
    public function searchById(Location $location) {
        return $this->query("SELECT * FROM he_locallization where he_id_locallization = ?", 
                $location->getIdlocation());
    }
}
?>

