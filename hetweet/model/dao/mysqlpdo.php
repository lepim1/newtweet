<?php
/**
 * PHP DATA OBJECT CLASS THAT CONNECTS TO MYSQL AND DOES QUERIES
 */
class MySQLPDO extends PDO {
    //--------------production localhost --------------
//    const DB_HOST = 'localhost';
//    const DB_PORT = '3306';
//    const DB_NAME = 'db_he_tweets';
//    const DB_USER = 'he_regularuser';
//    const DB_PASS = 'Q45yx6Wnc2RxSycL';
    
    /**
     * CONSTRUCT A PDO
     * @param type $options
     * @return null
     */
    public function __construct($options = null) {
        try {
//            --------------production-------------
//            parent::__construct('mysql:host='.MySQLPDO::DB_HOST.';port='.MySQLPDO::DB_PORT.';dbname='.MySQLPDO::DB_NAME,
//            MySQLPDO::DB_USER, MySQLPDO::DB_PASS, $options);
            
            //--------------operation--------------
            $url = parse_url(getenv("CLEARDB_DATABASE_URL"));
            $server = $url["host"];
            $username = $url["user"];
            $password = $url["pass"];
            $db = substr($url["path"], 1);
            
            //echo "criou o dao na classe dao";
            
            $dsn = 'mysql:dbname=' . $db . ';host=' . $server;                        
            parent::__construct($dsn, $username, $password,$options);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //echo "criou o dao na classe pdo"; 
            
            //$statm = $this->prepare("SELECT * FROM he_locallization"); 
            //$statm->execute();
            //print_r($statm);
            //echo "Aqui2 ".$statm->rowCount();
        } catch (PDOException $err) {
            echo $err->getMessage();
            return null;
        }
    }

    /**
     * MAKE SAFE QUERIES WITH PARAMETERS
     * @param type $query
     * @return type
     */
    public function query($query) { //SAFE QUERY WITH PREPARE STATEMENT                
        $args = func_get_args(); //PUTS THE ARGUMENTS PASSED TO THIS METHOD INTO AN ARRAY
        array_shift($args); //array_shift() TAKES OFF AND RETURNS THE FIRST ELEMENT(QUERY) OF THE ARRAY OF ARGUMENTS
        $statm = parent::prepare($query); //PREPARE THE QUERY
        $statm->execute($args); //EXECUTE THE STATEMENT
        return $statm;
    }    
}
?>

