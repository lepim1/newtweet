<?php

include_once 'mysqlpdo.php';

/**
 * CLASS TO MANIPULATE THE DATABASE WITH REGARDING TO PERFIL OPERATIONS
 *
 * @author leonardopimentelferreira
 */
class PerfilDAOMySQL extends MySQLPDO {

    public function __construct($driver_options = array(PDO::ATTR_PERSISTENT => false, PDO::MYSQL_ATTR_INIT_COMMAND =>  'SET NAMES utf8')) {
        parent::__construct($driver_options);
    }

    public function add(Perfil $perfil) {          
        return $this->query("INSERT INTO he_perfil (he_full_name, he_relationship, he_photo_server_path, he_id_login, he_who_i_am, he_id_locallization) VALUES (?, ?, ?, ?, ?, ?)", 
                $perfil->getFullname(),
                $perfil->getRelationship(),
                $perfil->getPathphotoserver(),
                $perfil->getIdlogin(),
                $perfil->getWhoiam(),
                $perfil->getLocal());
    }
    
    public function searchByName($name) {
        $sql = "SELECT p.he_full_name, p.he_id_perfil, loc.he_locallization FROM "
                . "he_perfil p, he_locallization loc WHERE "
                . "p.he_full_name like ? and "
                . "p.he_id_locallization = loc.he_id_locallization LIMIT 10";            
        //PREPARED STATEMENT
        $stmt = $this->prepare($sql);        
        $stmt->bindValue(1, "%$name%", PDO::PARAM_STR);                
        $stmt->execute();
        return $stmt;                
    }
    
    public function getIdPerfilNameByIdLogin(Perfil $perfil) {
        return $this->query("SELECT he_id_perfil, he_full_name FROM he_perfil where he_id_login = ?", 
                $perfil->getIdLogin());
    }
    
    public function getPerfilById(Perfil $perfil) {
        return $this->query("SELECT * FROM he_perfil where he_id_perfil = ?", 
                $perfil->getIdperfil());
    }
    
    public function followPerfil(Following $following){
        return $this->query("INSERT INTO he_perfil_follows_he_perfil "
                . "(he_id_perfil_follower, he_id_perfil_followed) VALUES (?, ?)", 
                $following->getIdperfilfollower(),
                $following->getIdperfilfollowed());
    }
    
    public function unFollowPerfil(Following $following){
        return $this->query("DELETE FROM he_perfil_follows_he_perfil WHERE "
                . "he_id_perfil_follower = ? and he_id_perfil_followed = ?", 
                $following->getIdperfilfollower(),
                $following->getIdperfilfollowed());
    }
    
    public function isFollowing($perfilfollower, $perfilfollowed){
        return $this->query("SELECT COUNT(*) FROM he_perfil_follows_he_perfil "
                . "WHERE he_id_perfil_follower = ? and he_id_perfil_followed = ?", 
                $perfilfollower,
                $perfilfollowed);
    }
    
    public function sendNotificationPerfil(Notification $notification){
        return $this->query("INSERT INTO he_perfil_notifies_he_perfil "
                . "(he_id_perfil_from, he_id_perfil_to, he_type_notification, he_date_time, he_status) VALUES (?, ?, ?, ?, ?)", 
                $notification->getPerfilfrom()->getIdperfil(),
                $notification->getPerfilto()->getIdperfil(),
                $notification->getTypenotification(),
                $notification->getDatetime(),
                $notification->getStatus());
    }
    
    public function getNotificationsByPerfil(Perfil $perfil){
        return $this->query("SELECT n.*, p.he_full_name, p.he_id_perfil "
                . "FROM `he_perfil_notifies_he_perfil` n, he_perfil p "
                . "WHERE `he_id_perfil_to` = ? and n.he_id_perfil_from = p.he_id_perfil ORDER BY n.he_date_time DESC",                 
                $perfil->getIdperfil());
    }
    
    public function getNewNotificationsByPerfil(Perfil $perfil, $datetime){
        return $this->query("SELECT n.*, p.he_full_name, p.he_id_perfil "
                . "FROM `he_perfil_notifies_he_perfil` n, he_perfil p "
                . "WHERE `he_id_perfil_to` = ? and he_date_time >= ? and n.he_id_perfil_from = p.he_id_perfil ORDER BY n.he_date_time DESC",                 
                $perfil->getIdperfil(),
                $datetime);
    }
    
    public function likeTweet(Like $like){
        return $this->query("INSERT INTO he_perfil_likes_tweet "
                . "(he_perfil_who_likes, he_id_tweet) VALUES (?, ?)", 
                $like->getIdperfilwholikes(),
                $like->getIdtweet());
    }
    
    public function unLikeTweet(Like $like){
        return $this->query("DELETE FROM he_perfil_likes_tweet WHERE "
                . "he_perfil_who_likes = ? and he_id_tweet = ?", 
                $like->getIdperfilwholikes(),
                $like->getIdtweet());
    }
    
    public function getCountFollowers(Perfil $perfil){
        return $this->query("SELECT COUNT(*) FROM he_perfil_follows_he_perfil WHERE "
                . "he_id_perfil_followed = ?", 
                $perfil->getIdperfil());
    }
    
    public function getCountFollowings(Perfil $perfil){
        return $this->query("SELECT COUNT(*) FROM he_perfil_follows_he_perfil WHERE "
                . "he_id_perfil_follower = ?", 
                $perfil->getIdperfil());
    }
    
    public function searchForLikes($idperfil){
        return $this->query("SELECT he_id_tweet FROM he_perfil_likes_tweet WHERE he_perfil_who_likes = ?", 
                $idperfil);
    }
       
}
?>

