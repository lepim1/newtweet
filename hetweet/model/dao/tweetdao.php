<?php
include_once 'mysqlpdo.php';
/**
 * CLASS TO MANIPULATE THE DATABASE WITH REGARDING TO TWEET OPERATIONS
 *
 * @author leonardopimentelferreira
 */
class TweetDAOMySQL extends MySQLPDO {
    public function __construct($driver_options = array(PDO::ATTR_PERSISTENT => true, PDO::MYSQL_ATTR_INIT_COMMAND =>  'SET NAMES utf8')) {
        parent::__construct($driver_options);
    }

    public function addNewTweet(Tweet $tweet) {
        return $this->query("INSERT INTO he_tweet (he_id_perfil, he_tweet, he_date_time) VALUES (?,?,?)", 
                $tweet->getIdperfil(), 
                $tweet->getTweet(), 
                $tweet->getDatetime());
    }
    
    public function addComment(Tweet $tweet) {
        return $this->query("INSERT INTO he_tweet (he_id_perfil, he_tweet, he_date_time, he_id_original_tweet) VALUES (?,?,?,?)", 
                $tweet->getIdperfil(), 
                $tweet->getTweet(), 
                $tweet->getDatetime(),
                $tweet->getIdoriginal());
    }
    public function searchForTweets(Perfil $perfil){
        return $this->query("SELECT * FROM he_tweet WHERE he_id_perfil = ? and he_id_original_tweet is NULL ORDER BY he_date_time DESC", 
                $perfil->getIdperfil());
    }
    public function searchForComments($idtweet){
        return $this->query("SELECT t.*, p.he_full_name 
            FROM he_tweet t, he_perfil p 
            WHERE t.he_id_original_tweet = ? AND
            t.he_id_perfil = p.he_id_perfil
            ORDER BY t.he_date_time ASC"
                ,$idtweet);
    }
}
?>