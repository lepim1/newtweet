<?php       
    $currentdirectory = dirname(__FILE__);   //same path as index.php    
    require dirname($currentdirectory).'/useful/session.php';
    $session = new Session();
    $session->sessionStart();
    if ($session->verifiesLogin() == true) {
        header("Location: mural.php");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php
            include_once dirname(__FILE__).'/head.html';
        ?>    
        <!-- CUSTOM STYLES FOR THIS TEMPLATE -->
        <link href="../bootstrap/css/starter-template.css" rel="stylesheet">        
        <title>HE:tweet</title>      
    </head>
    <body onload="checkURL()">
        <?php
            include_once $currentdirectory.'/navbarloginform.html';
        ?>
        <div class="container">
            <?php
                include_once $currentdirectory.'/divstartertemplate.html';
            ?>
            <div class="bs-example" data-example-id="block-btns">
                <div class="well forgot">            
                    <a href="./signup.php" class="btn btn-primary btn-lg btn-block">Cadastre-se</a>
                    <button onclick="showemailbox()" class="btn btn-default btn-lg btn-block">Esqueci minha senha</button>
                    <div id="forgot" class="hide well" style="border:none">
                        <form id="forgotForm" name="forgotForm" class="form-block" method="post" action="../controller/clogin.php" >
                            <div class="form-group"> 
                                <input id="emailforgot" name="email" type="text" placeholder="Digite o email para a recuperação" class="form-control" value="">
                            </div>
                            <button id="submitforgot" type="submit" class="btn btn-primary" data-loading-text="Enviando email...">Recuperar senha</button>
                            <input name="op" type="hidden" value="forgot">
                        </form>
                    </div><!-- /.hide well -->
                </div><!-- /.well -->          
            </div><!-- /.bs-example -->
        </div><!-- /.container -->

        
        <!-- Modal for login error -->
        <div id="modalloginerror" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header alert-info">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Desculpe</h4>
                    </div>
                    <div class="modal-body">
                        <p>Usuário/Email e/ou senha inválidos&hellip;</p>
                    </div>
                    <div class="modal-footer" style="border:none;">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <?php
            include_once $currentdirectory.'/foot.html';                        
            include_once $currentdirectory.'/bootstrap.html';
        ?>
        <script type="application/javascript"> 
            /**
            * BASIC FUNCTIONS FOR THE LOGIN PROCESS 
            * @returns {Boolean}
            */
            function validateLoginForm(){
                if(document.loginForm1.user.value == "" || document.loginForm1.password.value == ""){
                    showModalLoginError();
                   return false;
                }
                return true;
            }

            /**
             * CHECKS THE PARAM OF A URL AND DO THE CORRESPONDING ACTION
             * @returns {undefined}
             */
            function checkURL()
            {
                var url = window.location.search.substring(1);
                var params = url.split('&');
                for (var i = 0; i < params.length; i++) 
                {
                    var name = params[i].split('=');       
                    if (name[0] == "msg") 
                    {
                        if(name[1] == "welcome"){
                            $("#alertsignupok").removeClass('hide');
                        }else{
                            if(name[1] == "passok"){
                                $("#passok").removeClass('hide');
                            }
                        }
                    }       
                }
            }

            function validateEmail(email){                
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if(!re.test(email)){                    
                    return false;
                }
                return true;
            }

            function showModalLoginError(){
                $("#modalloginerror").modal({
                    show: true
                })
            }
           
            function showemailbox(){                
                $("#forgot").toggleClass("hide");                
            }   
            
            $("#submitforgot").click(function() {
                var $btn = $(this);                
                $btn.button('loading');
                var alertmailok = $("#alertemailok");
                var forgot = $("#forgot");
                $("#forgotForm").submit(function(e) {
                    e.preventDefault();    
                    if(validateEmail(document.forgotForm.email.value)){
                        var postData = $(this).serializeArray();
                        var formURL = $(this).attr("action");
                        var self = this;
                        $.ajax(
                        {
                            url : formURL,
                            type: "post",
                            data : postData
                        }).done(function(result) {                                   
                            var obj = JSON.parse(result);                            
                            if (obj.response === true){                                                                                            
                                $btn.button('reset');   
                                alertmailok.removeClass('hide');                                          
                                forgot.addClass('hide');                                
                                return true;
                            }else{                                
                                showModalLoginError();                                  
                            }  
                            $btn.button('reset');   
                            return false;                        
                        }).fail( function(xhr, textStatus, errorThrown) {
                            $btn.button('reset'); 
                            alert('Erro');
                            //alert(xhr.responseText);
                        });
                    }else{
                        showModalLoginError();
                    }                           
                 });
            });
             
             
            $("#loginForm1").submit(function(e) {                 
               e.preventDefault();    
               if(validateLoginForm()){
                   var postData = $(this).serializeArray();
                   var formURL = $(this).attr("action");
                   var self = this;
                   $.ajax(
                   {
                       url : formURL,
                       type: "post",
                       data : postData
                   }).done(function(result) {                       
                       var obj = JSON.parse(result);                        
                       if (obj.response === true){                            
                           self.submit();
                           window.location.href='mural.php'                               
                           return true;
                       }else{
                           showModalLoginError();                           
                       }
                       return false;
                   }).fail( function(xhr, textStatus, errorThrown) {
                        alert('Erro');
                        alert(xhr.responseText);
                        //document.write(xhr.responseText);
                   });
               }else{
                   showModalLoginError();
               }                           
            });   
            
            $(document).on("click", ".btalertok", function () {                 
                $('.alertok').addClass('hide');
            });
        </script>
        <script src="../bootstrap/js/searchbar.js"></script> 

    </body>
</html>
