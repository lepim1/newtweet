<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="./heicon.ico" type="image/x-icon">
    <!-- BOOTSTRAP CSS -->        
    <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">       
    <!-- CUSTOM STYLES FOR THIS TEMPLATE -->
    <link href="../bootstrap/css/starter-template.css" rel="stylesheet">       
    <title>Cadastro</title>
  </head>

  <body>
    <?php
        $currentdirectory = dirname(__FILE__);
        include_once $currentdirectory.'/navbarsignup.html';
    ?>

    <div class="container">
        <?php
            include_once $currentdirectory.'/divstartertemplate.html';
        ?>      
   
        <form id="signupForm" name="signupForm" class="well" method="post" action="../controller/clogin.php">
            <p class="text-center lead">Formulário de cadastro</p>
            <div class="form-group">
                <label for="email">Email</label>
                <input value="" type="email" class="form-control" id="email" name="email" placeholder="">                
            </div>
            <div class="form-group">
                <label>
                    <input id="agree" name="agree" type="checkbox"> Cheque esta caixa se o seu email estiver correto.
                </label>
            </div>
            <div class="form-group">
                <label for="password">Senha</label>
                <input value="" type="password" class="form-control" id="password" name="password" placeholder="">
                <span id="helpBlock" class="help-block">Sua senha deve possuir pelo menos 7 caracteres sendo letras minúsculas, maiúsculas e números</span>
            </div>
            <div class="form-group">
                <label for="password2">Redigite a sua senha</label>
                <input value="" type="password" class="form-control" id="password2" name="password2" placeholder="">
            </div>
            <div class="form-group">
                <label for="fullname">Nome Completo</label>
                <input value="" type="text" class="form-control" id="fullname" name="fullname" placeholder="">
            </div>
            <div class="form-group">
                <label for="username">Usuário (Opcional) </label>
                <input value="" type="text" class="form-control" id="username" name="username" placeholder="">
                <p class="help-block">Você poderá acessar o site com este usuário e também facilita que seus amigos o encontre</p>
            </div>
            <div class="form-group">
                <label for="local">Onde você mora</label>
                <input type="text" class="form-control" id="local" name="local" placeholder="Ex.: Uberlândia, MG, Brasil">
            </div>
            <div class="form-group">
                <label for="relationship">Relacionamento</label>
                <select id="relationship" name="relationship" class="form-control">
                    <option></option>
                    <option>Solteiro (a)</option>
                    <option>Namorando</option>
                    <option>Casado (a)</option>
                    <option>Viúvo (a)</option>
                </select>        
            </div>
            <div class="form-group">
                <label for="whoiam">Quem sou eu</label>
                <textarea class="form-control" id="whoiam" name="whoiam" placeholder="Fale um pouco sobre você"></textarea>
            </div>
            <div class="form-group inline-block">
                <div id="infocaptcha" class="hide alert alert-info" role="alert">Marque a opção "Eu não sou um robô"</div>
                <div id="g-recaptcha" class="g-recaptcha" data-sitekey="6Ld0WwATAAAAAH6xB9OlH0MLMnA3Og4H1HIoeeUZ" ></div>                
            </div>    
            <input type="hidden" name="op" id="op" value="signup">
            <button type="submit" class="btn btn-primary">Cadastrar</button>
        </form>
    </div>
      
    <?php   
        include_once $currentdirectory.'/foot.html';            
        include_once $currentdirectory.'/bootstrap.html';
    ?>
    <script src="../bootstrap/js/jquery.validate.min.js"></script>        
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="application/javascript">
        $.validator.addMethod("checkPass", function(value, element) {
            return checkPassword(value);
        }, "Sua senha deve conter pelo menos 7 caracteres sendo letras minúsculas, maiúsculas e números.");
        
        function checkPassword(str)
        {
            // AT LEAST 7 CHARACTERS, ONE NUMBER, ONE LOWERCASE AND ONE UPPERCASE            
            var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,}/;
            return re.test(str);            
        }   
        
        //CALLBACK HANDLER FOR SUBMIT
        $().ready(function() {             
            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {                            
                    fullname: {
                        required: true,
                        maxlength: 60
                    },				
                    password: {					
                        checkPass: true                        
                    },
                    password2: {					
                        equalTo: "#password"
                    },
                    email: {
                        required: true,
                        email: true,
                        maxlength: 60,
                        remote: {
                            url: '../controller/checkemail.php',
                            type: "post",
                            dataFilter: function(data) {
                                var json = JSON.parse(data);
                                if (json.result === true) {
                                    return true;
                                }
                                return false;
                            }                                          
                        }
                    },
                    username: {
                        maxlength: 45,
                        remote: {
                            url: '../controller/checkusername.php',
                            type: "post",
                            dataFilter: function(data) {                                
                                var json = JSON.parse(data);
                                if (json.result === true) {
                                    return true;
                                }
                                return false;
                            }

                        }
                    },
                    agree: "required",
                    whoiam: {
                        maxlength: 200
                    }
                },
                messages: {
                    fullname:{
                        required: "Por favor, diga o seu nome.",
                        maxlength: "Nome deve conter até 60 caracteres."
                    },				
                    password: {
                        checkPass: "Sua senha deve conter pelo menos 7 caracteres sendo letras minúsculas, maiúsculas e números."                            
                    },
                    password2: {
                            equalTo: "Senhas diferentes."
                    },
                    email: {
                        required: "Por favor, digite um email válido.",                                    
                        email: "Por favor, digite um email válido.",
                        remote: "Email já está cadastrado.",
                        maxlength: "Email deve conter até 60 caracteres."
                    },
                    username: {
                        maxlength: "Usuário deve conter até 45 caracteres.",
                        remote: "Usuário já existente, por favor escolha outro."
                    },                                
                    agree: "Por favor, cheque está caixa.",
                    whoiam: {
                        maxlength: "Até 200 caracteres."
                    }
                },
                submitHandler: function (form) {
                    var URL = $(form).attr('action') ;
                    $.ajax({
                        type: "post",
                        url: URL,
                        data: $(form).serialize(),
                    }).done(function(result) {                               
                        var obj = JSON.parse(result);                         
                        if(obj.response == "ok"){                            
                            window.location.href="login.php?msg=welcome";                                                                                    
                            return true;
                        }
                        if(obj.response == "ic"){
                            $("#infocaptcha").removeClass('hide');
                        }
                        return false;
                    });
                    return false; // required to block normal submit since you used ajax
                }
            });
        });        
        
        //GETS THE CURRENT USERS LOCATION: http://www.telize.com/
        function getgeoip(json){
            $("#local").val(json.city + ", " + json.region_code + ", " + json.country);
        }                     
    </script>
    <script type="application/javascript" src="https://www.telize.com/geoip?callback=getgeoip"></script>
  </body>
</html>