<?php
    include_once dirname(dirname(__FILE__)).'/useful/session.php';
    include_once dirname(dirname(__FILE__)).'/controller/clogin.php';
    $session = new Session();
    $session->sessionStart();    
    if($session->verifiesLogin() == true){
        header('Location: mural.php');
        exit;
    }
    if(isset($_GET["token"])){        
        $clogin = new CLogin();
        if(!$clogin->istokenvalid($_GET["token"])){            
            header("Location: login.php");
            exit;
        }        
    }else{
        header("Location: login.php");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php
            include_once dirname(__FILE__).'/head.html';
        ?>
        <!-- CUSTOM STYLES FOR THIS TEMPLATE -->
        <link href="../bootstrap/css/starter-template.css" rel="stylesheet">   
        <title>Redefinir senha</title>
    </head>
  <body>
    <?php
        include_once dirname(__FILE__).'/navbarsignup.html';
    ?>    
    <div class="container">
        <?php
            include_once dirname(__FILE__).'/divstartertemplate.html';
        ?>              
        <div id="forgot" class="well wellbordernone">  
            <h4 class="text-center">Redefina a sua senha</h4>
            <form id="forgotFormReset" name="forgotForm" class="form-block" method="post" action="../controller/clogin.php" >
                <div class="form-group">
                    <label for="password">Senha</label>
                    <input value="" type="password" class="form-control" id="password" name="password" placeholder="">
                    <span id="helpBlock" class="help-block">Sua senha deve possuir pelo menos 7 caracteres sendo letras minúsculas, maiúsculas e números</span>
                </div>
                <div class="form-group">
                    <label for="password2">Redigite a sua senha</label>
                    <input value="" type="password" class="form-control" id="password2" name="password2" placeholder="">
                </div> 
                <input value="<?php echo $_GET["token"]; ?>" type="hidden" id="tokenform" name="tokenform" >
                <input value="forgotreset" type="hidden" id="op" name="op" >                
                <button type="submit" class="btn btn-primary" >Alterar senha</button>
            </form>
        </div>         
    </div>                          

    <?php 
        include_once dirname(__FILE__).'/foot.html';            
        include_once dirname(__FILE__).'/bootstrap.html';
    ?>
    <script src="../bootstrap/js/jquery.validate.min.js"></script>        
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="application/javascript">        
        $.validator.addMethod("checkPass", function(value, element) {
            return checkPassword(value);
        }, "Username must contain only letters, numbers.");  

        function checkPassword(str)
        {
            // at least one number, one lowercase and one uppercase letter
            // at least seven characters that are letters and numbers
            var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,}/;
            return re.test(str);
        }  
        //callback handler for form submit
	$().ready(function() {             
            // validate signup form on keyup and submit
            $("#forgotFormReset").validate({
                rules: {                           		
                    password: {					
                        checkPass: true                        
                    },
                    password2: {					
                        equalTo: "#password"
                    }
                },
                messages: {								
                    password: {
                        checkPass: "Sua senha deve conter pelo menos 7 caracteres sendo letras minúsculas, maiúsculas e números."                        
                    },
                    password2: {
                        equalTo: "Senhas diferentes."
                    }                    
                }
            });
	});                                                                 
    </script>
  </body>
</html>