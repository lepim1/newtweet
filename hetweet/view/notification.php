<?php
    include_once dirname(dirname(__FILE__)).'/model/dao/perfildao.php';
    include_once dirname(dirname(__FILE__)).'/model/datamodel/mperfil.php';
    include_once dirname(dirname(__FILE__)).'/model/datamodel/mnotification.php'; 
    include_once dirname(dirname(__FILE__)).'/useful/session.php'; 
    include_once dirname(dirname(__FILE__)).'/useful/functions.php'; 
/**
 * Description of notification
 *
 * @author leonardopimentelferreira
 */
class UpdateNotification {
    //put your code here
    const GET_NEW_NOTIFICATIONS = 1;
    const GET_NOTIFICATIONS = 2;
    
    function __construct(){
        if(isset($_POST)){
            switch ($_POST["op"]){
                case UpdateNotification::GET_NEW_NOTIFICATIONS:
                    $this->getNotifications(UpdateNotification::GET_NEW_NOTIFICATIONS);
                    break;
                case UpdateNotification::GET_NOTIFICATIONS:
                    $this->getNotifications(UpdateNotification::GET_NOTIFICATIONS);
                    break;
            }
        } 
    }   

    function getNotifications($op){        
        $session = new Session();
        $session->sessionStart();  
        $islogged = $session->verifiesLogin();  
        $response = array("count"=>0);
        if($islogged){//and it is logged
            $perfil = new Perfil();
            $functions = new Functions();
            $perfil->setIdperfil($session->getVariableonsession("perfil"));            
            $perfildao = new PerfilDAOMySQL();                     
            if ($op == UpdateNotification::GET_NOTIFICATIONS) { 
                $statm = $perfildao->getNotificationsByPerfil($perfil);                           
            }elseif($op == UpdateNotification::GET_NEW_NOTIFICATIONS){
                $datetime = new DateTime($_POST["datelastupdate"]);                  
                $statm = $perfildao->getNewNotificationsByPerfil($perfil, $datetime->format("Y-m-d H:i:s"));  
            }
            $response["count"]=$statm->rowCount();
            $notifications = array();
            $notificationsaux = $statm->fetchAll();            
            foreach ($notificationsaux as $row) {
                $notification = new Notification;
                                
                $perfilfrom = new Perfil();
                $perfilfrom->setIdperfil($row['he_id_perfil_from']);
                $perfilfrom->setFullname($row['he_full_name']);
                $notification->setPerfilfrom($perfilfrom);
                
                $perfilto = new Perfil();
                $perfilto->setIdperfil($row['he_id_perfil_to']);
                $notification->setPerfilto($perfilto);

                $notification->setDatetime($row['he_date_time']);
                $notification->setIdnotification($row['he_id_notification']);                                
                $notification->setTypenotification($row['he_type_notification']);
                $notification->setStatus($row['he_status']);
                
                $notifications[] = $notification;                
                
            }       
            $response["html"]=$this->generateHTML($notifications, $op);            
        }                    
        echo json_encode($response);
        exit;       
    }
    
    function generateHTML($notifications, $op){
        $html = '';
        if($op == UpdateNotification::GET_NOTIFICATIONS){            
            if(count($notifications)==0){
                $html .= 'Você não possui notificações';
            }
        }                
        foreach ($notifications as $notification){
            $msg = "";
            switch ($notification->getTypenotification()){
                case "LIKE":
                    $msg = "Gostou do seu Tweet";
                    break;
                case "TWEET":
                    $msg = "Comentou seu Tweet";
                    break;
                case "FOLLOWING":
                    $msg = "Está te seguindo";
                    break;
                default:
                    break;                    
            }
            $html .= '<a class="list-group-item" href="mural.php?perfil='. $notification->getPerfilfrom()->getIdperfil().'">'
                        . '<div class="media" id="'.$notification->getPerfilfrom()->getIdperfil(). '">'
                            . '<div class="media-left">'
                                . '<span class="glyphicon glyphicon-user" aria-hidden="true" style="font-size: 2.5em;"></span>'
                            . '</div>'
                            . '<div class="media-body">'
                                . $notification->getPerfilfrom()->getFullname()
                                . '<br><small>'.$msg.'</small>'
                            . '</div>'
                        . '</div>'
                   .'</a>';
        }
        return $html;
    }
}
$updatenotification = new UpdateNotification;
?>