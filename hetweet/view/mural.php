<?php    
    include_once dirname(dirname(__FILE__)).'/useful/session.php';
    include_once dirname(dirname(__FILE__)).'/controller/cmural.php';
  
    #TYPE OF USER
    define("VISITOR",1);
    define("LOG",2);
    define("LOGVISITOR",3);
        
    #VERIFIES USER LOGIN STATUS  
    $session = new Session();
    $session->sessionStart();  
    $islogged = $session->verifiesLogin();
    
    $cmural = new CMural();
    $perfil = new Perfil();
        
    #VERIFIES WHAT TYPE OF USER
    if(isset($_GET["perfil"])){//if there is a mural to search for
        $perfil->setIdperfil($_GET["perfil"]);
        $perfil = $cmural->getPerfilById($perfil);        
        if($perfil->getIdperfil() == NULL){//if a perfil has not been found 
            if(!$islogged){//and user is not logged
                header("Location: login.php");
                exit;               
            }else{
                $perfil->setIdperfil($session->getVariableonsession("perfil"));
                $cmural->getPerfilById($perfil);
                $kindofuser = LOG;
            }
        }else{//if a perfil has been found
            if($islogged){//and it is logged
                if($session->getVariableonsession("perfil") == $perfil->getIdperfil()){
                    $kindofuser = LOG;
                }else{
                    $kindofuser = LOGVISITOR;
                    $isfollowing = $cmural->isFollowing($session->getVariableonsession("perfil"), $perfil->getIdperfil());
                }
            }else{//and it is not logged
                $kindofuser = VISITOR;
            }                        
        }
    }else{ //if there is no mural to search for
        if($islogged){//and it is logged in
            $perfil->setIdperfil($session->getVariableonsession("perfil"));
            $cmural->getPerfilById($perfil);
            $kindofuser = LOG;
        }else{//no mural to search for and not logged in
            header("Location: login.php");
            exit;
        }
    }
    $local = $cmural->getLocation($perfil);  
    $follow = $cmural->getCountFollowersAndFollowings($perfil);
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <?php   
        include_once dirname(__FILE__).'/head.html';
    ?>           
    <!-- CUSTOM STYLES FOR THIS TEMPLATE -->
    <link href="../bootstrap/css/blog-template.css" rel="stylesheet">
    <link href="../bootstrap/css/starter-template.css" rel="stylesheet">    
    <title>Mural</title>
  </head>
  <body>
    <?php
        //include_once $currentdirectory.'/navbarsignup.html';
    ?>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="login.php">HE:tweets</a>                    
            </div><!-- /.navbar-header --> 
            <div class="navbar-collapse collapse">                
                 <ul class="nav navbar-nav navbar-right nav-pills">  
                    <?php if($kindofuser == LOG or $kindofuser == LOGVISITOR){?>
                    <li data-toggle="tooltip" title="Notificações" data-placement="bottom"><a href="javascript:void(0)" role="button" data-placement="bottom" class="popupnotifications pop" data-trigger="manual" data-poload="notification.php" >
                        <span class="glyphicon glyphicon-bullhorn" aria-hidden="true" ></span><span id="numnotifications" class="badge progress-bar-success"></span></a>
                    </li>                                          
                    <li data-toggle="tooltip" title="Meu mural" data-placement="bottom"><a href="mural.php" role="button"><span class="glyphicon glyphicon-home" aria-hidden="true" ></span></a>
                    </li>
                    <li data-toggle="tooltip" title="Sair do sistema" data-placement="bottom"><a href="../controller/clogin.php?op=signout" role="button">Sair</a></li>                                            
                    <?php }else{if($kindofuser == VISITOR){?>
                    <li><a id="signup" href="signup.php" role="button" >Registrar</a></li>                                          
                    <li><a id="signin" href="login.php" role="button">Entrar</a></li>
                    <?php }}?>
                 </ul>   
                <form id="formsearchuser" style="margin-top: 8px; margin-bottom: 8px; max-width: 75%">
                    <div style="display:table;" class="input-group">
                        <span style="width: 1%;" class="input-group-addon" data-toggle="tooltip" title="Encontre seus amigos" data-placement="bottom"><span class="glyphicon glyphicon-search"></span></span>
                        <input id="user" name="user" type="text" autofocus="autofocus" autocomplete="off" placeholder="Pesquise por nome ou email (Digite pelo menos 3 caracteres)" class="form-control popupsearchusers pop"
                            title="Resultados" data-placement="bottom" data-trigger="manual" data-poload="../controller/searchuser.php">
                    </div>
                </form>
            </div> <!-- /.navbar --> 
        </div><!-- /.container -->
    </nav><!-- /.navbar navbar-inverse navbar-fixed-top -->
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 col-md-3 col-lg-2 sidebar">
                <div class="thumbnail">
                    <img data-src="holder.js/100%x200" alt="Generic placeholder thumbnail">
                    <div class="caption" >
                        <h3 id="fullname"><?php echo $perfil->getFullname();?></h3>
                        <span><?php echo $local->getLocation();?></span>
                        <h5><?php echo $perfil->getRelationship();?></h5>
                        <p><?php echo $perfil->getWhoiam();?></p>            
                        <input id="perfil" type="hidden" value="<?php echo $perfil->getIdperfil(); ?>">
                    </div><!-- ./caption -->
                </div><!-- ./thumbnail -->
                <ul class="nav nav-sidebar">                       
                    <li>
                        <div>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Seguidores"><span class="glyphicon glyphicon-resize-small" aria-hidden="true" ></span> <?php echo $follow[0];?></a>
                        </div>
                        <div>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Seguindo"><span class="glyphicon glyphicon-resize-full" aria-hidden="true"></span> <?php echo $follow[1];?></a>
                        </div>               
                    </li>            
                    <?php if($kindofuser==LOGVISITOR){ ?>                    
                    <?php if($isfollowing){ ?>                                        
                    <li><a href="javascript:void(0)" id="unfollow" class="greenlink" ><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Seguindo (clique para não seguir)</a></li>                                 
                    <li><a href="javascript:void(0)" id="follow" style="display: none;"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Clique para seguí-lo </a></li>
                    <?php }else{ ?>
                    <li><a href="javascript:void(0)" id="unfollow" class="greenlink" style="display: none;"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Seguindo (clique para não seguir) </a></li>                                 
                    <li><a href="javascript:void(0)" id="follow"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Clique para seguí-lo </a></li>
                    
                    <?php }} ?>
                </ul>
            </div> <!-- ./col-sm-3 col-md-2 sidebar -->            
            <div class="col-sm-9 col-sm-offset-4 col-md-10 col-md-offset-3 col-lg-10 col-lg-offset-2 main" id="blog">  
                <?php if($kindofuser == LOG){?>
                <div class="messageform" >
                    <form id="formsendmessage" class="form-block" method="post">
                        <label for="postmessage" class=" control-label">Mensagem:</label>
                        <div class="input-group">
                            <textarea id="postmessage" name="postmessage" class="form-control custom-control" rows="3" style="resize:none"></textarea>     
                            <span id="btsendmessage" class="input-group-addon btn btn-primary">
                                Postar</span>
                        </div>                                                
                    </form>
                </div><!-- ./messageform -->
                <h2 class="page-header">Veja as novidades do seu mural:</h2>
            <?php } else { ?>
                <h2 class="page-header">Veja o mural de <?php echo $perfil->getFullname(); ?></h2>
            <?php }?>
            </div>
        </div>
    </div>
    <?php        
        include_once dirname(__FILE__).'/bootstrap.html';        
    ?>    
    <script src="../bootstrap/js/holder.js"></script>    
    <script src="../bootstrap/js/mural.js"></script> 
    <script src="../bootstrap/js/searchbar.js"></script> 
 </body>
</html>